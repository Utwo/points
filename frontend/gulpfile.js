'use strict';

let gulp = require('gulp');
let autoprefixer = require('gulp-autoprefixer');
let concat = require('gulp-concat');
let uglify = require('gulp-uglify');
let es = require('event-stream');
let sass = require('gulp-sass');
let cleanCSS = require('gulp-clean-css');
let sourcemaps = require('gulp-sourcemaps');

// Configuration
let configuration = {
    src: {
        html: './src/*.html',
        cssFolder: [
            './src/scss/'
        ],
        cssMain: 'main.scss',
        jsFolder: [
            './src/js'
        ],
        jsMain: '/main.js',
        image: './src/img',
        dev: 'src'
    },
    build: './public/'
};

// Task css
gulp.task('css', function () {
    let mainCss = gulp.src([configuration.src.cssFolder + configuration.src.cssMain])
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(autoprefixer('last 2 version'))
        .pipe(sourcemaps.init())
        .pipe(cleanCSS())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(configuration.build + 'css/'))
});

gulp.task('watch', function () {
    gulp.watch(configuration.src.cssFolder + '/**', ['css']);

});

// The default task (called when you run `gulp` from cli)
gulp.task('default', ['css', 'watch']);

// Task when ready for production
gulp.task('production', ['css']);