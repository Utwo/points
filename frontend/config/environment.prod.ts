export const environment = {
    production: true,
    URL: 'http://pip-utwo.rhcloud.com',
    PORT: 80,
    API_VERSION: 'api/v1',
    POINTS_PER_SEC: 3000
};
