export const environment = {
    production: false,
    URL: 'http://localhost',
    PORT: 3333,
    API_VERSION: 'api/v1',
    POINTS_PER_SEC: 3000
};
