import {bootstrap} from '@angular/platform-browser-dynamic';
import {enableProdMode} from '@angular/core';
import {AppComponent, environment} from './app/';
import {LoggedInGuard} from "./app/shared/logged-in.guard";
import {APP_ROUTES_PROVIDER} from "./app/routes";
import {HTTP_PROVIDERS} from "@angular/http";
import {UserService} from "./app/shared/user.service";
import {BubbleService} from "./app/shared/bubble.service";
import {Title} from "@angular/platform-browser";

if (environment.production) {
    enableProdMode();
}


bootstrap(AppComponent, [
    APP_ROUTES_PROVIDER,
    LoggedInGuard,
    BubbleService,
    UserService,
    Title,
    HTTP_PROVIDERS
]);
