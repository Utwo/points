import {Component} from '@angular/core';
import {ROUTER_DIRECTIVES} from '@angular/router'
import {BubbleComponent} from "./shared/bubble/bubble.component";

@Component({
    moduleId: module.id,
    selector: 'app-root',
    directives: [ROUTER_DIRECTIVES, BubbleComponent],
    templateUrl: 'app.component.html',
})
export class AppComponent {
    title = 'Points';
}
