import {Component, OnInit, OnDestroy} from '@angular/core';
import {DotsComponent} from "./dots/dots.component";
import {ProfileComponent} from "./profile/profile.component";
import {TopScoreComponent} from "./top-score/top-score.component";
import {PointService} from "./shared/point.service";

@Component({
    moduleId: module.id,
    selector: 'home',
    templateUrl: 'home.component.html',
    directives: [DotsComponent, ProfileComponent, TopScoreComponent],
    providers: [PointService]
})

export class HomeComponent implements OnInit, OnDestroy {

    constructor(private pointService:PointService) {
    }

    ngOnInit() {
        this.pointService.socketConnect()
    }

    ngOnDestroy() {
        this.pointService.socketDisconnect()
    }
}