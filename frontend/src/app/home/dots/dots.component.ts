import {Component, OnInit, OnDestroy} from '@angular/core';
import {environment} from '../../environment'

@Component({
    moduleId: module.id,
    selector: 'dots',
    templateUrl: 'dots.component.html'
})
export class DotsComponent implements OnInit, OnDestroy {
    private points:String = '';
    private points_interval;


    constructor() { }

    ngOnInit() {
        this.appendPoints()
    }

    ngOnDestroy():any {
        clearInterval(this.points_interval)
    }

    appendPoints() {
        this.points_interval = setInterval(() => {
            this.points += '.';
        }, environment.POINTS_PER_SEC)
    }
}