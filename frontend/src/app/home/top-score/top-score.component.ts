import {Component, OnInit, OnDestroy} from '@angular/core';
import {PointService} from "../shared/point.service";

@Component({
    moduleId: module.id,
    selector: 'top-score',
    templateUrl: 'top-score.component.html'
})
export class TopScoreComponent implements OnInit, OnDestroy {
    private topScore;

    constructor(private pointService: PointService) { }

    ngOnInit() {
        this.getTopScore()
    }

    ngOnDestroy() {

    }

    getTopScore() {
        this.pointService.topScore().subscribe(result => {
            this.topScore = result
        })
    }
}