import {Component, OnDestroy, OnInit} from '@angular/core'
import {environment} from "../../environment"
import {UserService} from "../../shared/user.service";
import {Title} from "@angular/platform-browser";

@Component({
    moduleId: module.id,
    selector: 'profile',
    templateUrl: 'profile.component.html'
})

export class ProfileComponent implements OnInit, OnDestroy {
    private auth_user;
    private points_number;
    private score_interval;

    constructor(private userService: UserService, private titleService: Title) {
    }

    ngOnInit(): any {
        this.getScore()
        this.incrementScore()
    }

    ngOnDestroy() {
        clearInterval(this.score_interval)
    }

    getScore() {
        this.userService.getProfile().subscribe(user_profile => {
            if (user_profile) {
                this.auth_user = user_profile
                this.points_number = user_profile.score
            }
        })
    }

    incrementScore() {
        this.score_interval = setInterval(() => {
            this.points_number = this.points_number + 1;
            this.titleService.setTitle(this.points_number);
        }, environment.POINTS_PER_SEC)
    }

    onLogout() {
        this.userService.logout()
    }
}