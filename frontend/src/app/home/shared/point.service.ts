import {Injectable} from '@angular/core'
import {environment} from '../../environment'
import {Observable} from "rxjs/Rx";
import * as io from 'socket.io-client';
import {UserService} from "../../shared/user.service";

@Injectable()
export class PointService {
    private url: String = environment.URL + ':' + environment.PORT;
    private socket;

    constructor(private userService: UserService) {
    }

    socketConnect() {
        let jwt = localStorage.getItem('auth_token')
        this.socket = io.connect(this.url);
        this.socket.on('connect', function (socket) {
                this.on('authenticated', function () {
                }).emit('authenticate', {token: jwt});
            });
        this.socket.on("unauthorized", ((error) => {
            if (error.data.type == "UnauthorizedError" || error.data.code == "invalid_token") {
                this.socketDisconnect()
                this.userService.logout()
            }
        }));
        this.socket.on('connect_error', (error) => {
            this.userService.logout()
        })
    }

    topScore() {
        let observable = new Observable(observer => {
            this.socket.on('top-score', function (data) {
                observer.next(data);
            });
            return () => {
                this.socket.disconnect();
            };
        })
        return observable;
    }

    socketDisconnect() {
        this.socket.disconnect()
    }
}