import {Component} from '@angular/core';
import {UserService} from "../shared/user.service";
import {UnicornComponent} from "../shared/asci/unicorn.component";
import {ROUTER_DIRECTIVES, Router} from "@angular/router";

@Component({
    moduleId: module.id,
    selector: 'signup',
    templateUrl: 'signup.component.html',
    directives: [ROUTER_DIRECTIVES, UnicornComponent]
})
export class SignupComponent {

    constructor(private userService:UserService, private router:Router) {
    }

    onSubmit(credentials) {
        this.userService.signup(credentials.email, credentials.username, credentials.password).subscribe(result => {
            if (result) {
                this.router.navigate(['/login'])
            }
        })
    }

}
