import {Component} from '@angular/core'
import {Router, ROUTER_DIRECTIVES} from "@angular/router";
import {FORM_DIRECTIVES} from '@angular/common';

import {UserService} from "../shared/user.service";
import {UnicornComponent} from "../shared/asci/unicorn.component";

@Component({
    moduleId: module.id,
    selector: 'login',
    templateUrl: 'login.component.html',
    directives: [FORM_DIRECTIVES, ROUTER_DIRECTIVES, UnicornComponent]
})

export class LoginComponent {
    constructor(private userService:UserService, private router:Router) {
    }

    onSubmit(credentials) {
        this.userService.login(credentials.email, credentials.password).subscribe(result => {
            if (result) {
                this.router.navigate(["/"]);
            }
        })
    }
}