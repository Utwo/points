import { Component } from '@angular/core';
import {ROUTER_DIRECTIVES} from "@angular/router";

@Component({
  moduleId: module.id,
  selector: 'app-not-found',
  templateUrl: 'not-found.component.html',
  directives: [ROUTER_DIRECTIVES]
})
export class NotFoundComponent {
}
