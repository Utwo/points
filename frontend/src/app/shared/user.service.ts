import {Injectable} from '@angular/core'
import {Http, Headers} from '@angular/http'
import {environment} from '../environment';
import {Observable} from "rxjs/Rx";
import {Router} from "@angular/router";
import {BubbleService} from "./bubble.service";

@Injectable()
export class UserService {
    private loggedIn: boolean = false;

    constructor(private http: Http, private router: Router, private bubbleService: BubbleService) {
        this.loggedIn = !!localStorage.getItem('auth_token')
    }

    login(email, password) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json')

        return this.http.post(environment.URL + ':' + environment.PORT + '/login', JSON.stringify({
            'email': email,
            'password': password
        }), {headers})
            .map(res => res.json())
            .map(res => {
                localStorage.setItem("auth_token", res.token)
                this.loggedIn = true;
                return true;
            }).catch(this.handleError.bind(this));
    }

    signup(email, username, password) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json')

        return this.http.post(environment.URL + ':' + environment.PORT + '/register', JSON.stringify({
            'email': email,
            'username': username,
            'password': password
        }), {headers})
            .map(res => res.json())
            .map(res => {
                this.bubbleService.newBubble("Watch the unicorn! We will review your soul.")
                return true;
            }).catch(this.handleError.bind(this));
    }

    getProfile(user_id = null) {
        let headers = new Headers()
        user_id = (user_id) ? user_id : localStorage.getItem('auth_token')
        headers.append('Authorization', 'Bearer ' + user_id)
        let profile: any;
        profile = this.http.get(environment.URL + ':' + environment.PORT + '/api/v1' + '/profile', {headers})
            .map((res: any) => res.json()).catch(this.handleError.bind(this));
        return profile;
    }

    logout() {
        localStorage.removeItem("auth_token")
        this.loggedIn = false
        this.router.navigate(['/login'])
    }

    isLoggedIn(): boolean {
        return this.loggedIn
    }

    handleError(error: any) {
        error = error.json()
        let errMsg = (error.message) ? error.message :
            (error[0].message) ? error[0].message : 'Server error';
        if (errMsg) {
            this.bubbleService.newBubble(errMsg)
        }
        if (error.status == 401) {
            this.logout()
        }
        return Observable.throw(errMsg);
    }

}