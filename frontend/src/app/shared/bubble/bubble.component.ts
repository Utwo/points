import {Component, OnInit} from '@angular/core';
import {BubbleService} from "../bubble.service";

@Component({
    moduleId: module.id,
    selector: 'bubble-component',
    templateUrl: 'bubble.component.html',
})
export class BubbleComponent {

    private message_text = '';
    private showText:boolean = false;

    constructor(private bubbleService:BubbleService) {
        this.bubbleService.bubbleRequest.subscribe(data => {
            this.message_text = '';
            this.showText = false;
            this.changeText(data)
        })
    }

    changeText(text) {
        let interval = setInterval(() => {
            this.showText = true;
            if (text[0]) {
                this.message_text += text[0]
                text = text.substring(1)
            } else {
                clearInterval(interval)
            }
        }, 80)
    }

}