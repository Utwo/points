import {Injectable, EventEmitter} from "@angular/core";

@Injectable()
export class BubbleService {

    public bubbleRequest = new EventEmitter();

    public newBubble(message){
        return this.bubbleRequest.emit(message);
    }
}
