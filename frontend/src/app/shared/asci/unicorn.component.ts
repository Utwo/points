import {Component} from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'unicorn-asci',
    templateUrl: 'unicorn.component.html',
})
export class UnicornComponent {}
