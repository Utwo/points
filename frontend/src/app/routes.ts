import {provideRouter} from '@angular/router'

import {LoggedInGuard} from "./shared/logged-in.guard";
import {LoginComponent} from "./login/login.component";
import {HomeComponent} from "./home/home.component";
import {SignupComponent} from "./signup/signup.component";
import {NotFoundComponent} from "./not-found/not-found.component";

export const routes = [
    {path: '', component: HomeComponent, as: 'home', canActivate: [LoggedInGuard], useAsDefault: true},
    {path: 'login', component: LoginComponent, as: 'login'},
    {path: 'register', component: SignupComponent, as: 'register'},
    {path: '404', name: 'NotFound', component: NotFoundComponent},
    {path: '**', redirectTo: '404'}
];

export const APP_ROUTES_PROVIDER = provideRouter(routes)

