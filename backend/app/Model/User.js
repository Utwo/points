'use strict'

const Lucid = use('Lucid')

class User extends Lucid {

    static get hidden() {
        return ['password']
    }

    apiTokens() {
        return this.hasMany('App/Model/Token')
    }

   	 static get register_rules () {
        return {
            email: 'required|email|unique:users',
            username: 'required|unique:users|min:2|max:12',
            password: 'required|min:2|max:12',
        }
    }

}

module.exports = User
