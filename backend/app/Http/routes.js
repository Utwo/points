'use strict'

/*
|--------------------------------------------------------------------------
| Router
|--------------------------------------------------------------------------
|
| AdonisJs Router helps you in defining urls and their actions. It supports
| all major HTTP conventions to keep your routes file descriptive and
| clean.
|
| @example
| Route.get('/user', 'UserController.index')
| Route.post('/user', 'UserController.store')
| Route.resource('user', 'UserController')
*/

const Route = use('Route')

Route.group('v1', function () {
    Route.get('/profile/:user_id?', 'UserController.profile').middleware('auth:jwt')
    Route.get('/top-score', 'DotController.response_top_score').middleware('auth:jwt')
}).prefix('/api/v1')

Route.on('/').render('welcome')
Route.post('/login', 'UserController.login')
Route.post('/register', 'UserController.register')
