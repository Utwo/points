'use strict'
const socketioJwt = require('socketio-jwt');
const Env = use('Env');
const Redis = use('Redis');
const Config = use('Config')
const DotController = use('App/Http/Controllers/DotController')
const co = require('co');

const ctrl = new DotController()
let top_score_users = []

const update_points = co.wrap(function*(user_id) {
    return yield ctrl.update_points(user_id)
});

const get_top_score = co.wrap(function*(top_score_users = []) {
    return yield ctrl.get_top_score(top_score_users)
});

function remove_from_array(array, number) {
    const index = array.indexOf(number);
    if (index > -1) {
        array.splice(index, 1);
    }
}

module.exports = function (server) {

    const io = use('socket.io')(server)
    let connected_users = [];

    io.on('connection', socketioJwt.authorize({
        secret: Env.get('APP_KEY'),
        timeout: 10000 // 10 seconds to send the authentication message
    })).on('authenticated', function (socket) {
        //this socket is authenticated, we are good to handle more events from it.
        const user_id = socket.decoded_token.payload;
        let refresh_point;
        console.log('user ' + user_id + ' connected');

        if(! connected_users.find((u) => user_id == u)){
            refresh_point = setInterval(() => {
                update_points(user_id)
            }, Config.get('score.increment_per_second'))
        }

        connected_users.push(user_id)

        socket.on('disconnect', function () {
            console.log('user ' + user_id + ' disconnected');
            clearInterval(refresh_point);
            remove_from_array(connected_users, user_id)
            socket.disconnect();
        });

        /* var redis_handler = function(subscribed, channel, message) {
         if (channel == 'user.' + socket.decoded_token.sub) {
         message = JSON.parse(message);
         console.log('write to ' + channel + ' channel');
         io.emit(channel + ':' + message.event, message.data);
         }
         };

         Redis.on("pmessage", redis_handler);*/
    });
    let refresh_top_score = setInterval(() => {
        get_top_score(top_score_users).then(top_score => {
            top_score_users = top_score
            io.emit('top-score', top_score);
        })
    }, Config.get('score.increment_per_second'))

    /*Redis.psubscribe('*', function (err, count) {
     //console.log('psubscribe');
     });*/

}
