'use strict'
const Redis = use('Redis');
const User = use('App/Model/User')
const Config = use('Config')
var _ = require('lodash')

class DotController {
    * update_points(user_id) {
        //yield Redis.incr(user_id);
        let score = yield Redis.zincrby('points_list', 1, user_id)
        /*const dot = yield Dot.findBy('user_id', user_id);
         dot.point = ++dot.point;
         yield dot.save();*/
        return score
    }

    * get_top_score(top_users = null) {
        var users_score = []
        let redis_id_score = yield Redis.zrevrange('points_list', 0, Config.get('score.top_score_count'), 'withscores')
        for (let i = 0; i < redis_id_score.length; i += 2) {
            users_score.push({
                id: parseInt(redis_id_score[i]),
                score: parseInt(redis_id_score[i + 1])
            });
        }
        let users = yield this.get_users_by_score(top_users, users_score)
        return users
    }

    * response_top_score(request, response) {
        let top_score = yield this.get_top_score()
        response.json(top_score)
        return
    }

    * get_users_by_score(top_users, users_score) {
        let new_top_users = [];
        let new_user;
        for (let score of users_score) {
            let removed_user = _.remove(top_users, (user) => {
                return user.id == score.id
            })
            if (removed_user.length == 1) {
                new_user = removed_user[0]
            } else {
                new_user = yield User.find(score.id)
                new_user = new_user.toJSON();
            }
            new_user.score = score.score
            new_top_users.push(new_user)
        }
        return new_top_users;
    }
}

module.exports = DotController
