'use strict'
const User = use('App/Model/User')
const Hash = use('Hash')
const Redis = use('Redis')
const Validator = use('Validator')
const md5 = require('md5')

class UserController {
  * login(request, response) {
    const email = request.input('email')
    const user = yield User.findBy('email', email)
    if (user != null) {
      const password = request.input('password')
      const isSame = yield Hash.verify(password, user.password)
      if (isSame && user.active) {
        const token = yield request.auth.generate(user)
        response.json({user: user, token: token})
        return
      }
    }

    response.unauthorized('Invalid credentails')
  }

  * profile(request, response) {
    let user;
    if (request.param('user_id')) {
      user = yield User.find(request.param('user_id'))
    } else {
      user = request.authUser;
      user.profile_image = yield this.get_gravatar_link(user.email)
    }
    if (user) {
      let score = yield Redis.zscore('points_list', user.id)
      user = user.toJSON();
      user.score = score ? parseInt(score) : 0;
      response.json(user)
      return
    }
    response.notFound('User not found')
    return
  }

  * register(request, response) {
    const user_data = request.all()
    const validation = yield Validator.validate(user_data, User.register_rules)

    if (validation.fails()) {
        response.badRequest(validation.messages())
        return
    }

    let new_user = new User();
    new_user.email = user_data.email;
    new_user.username = user_data.username;
    new_user.password = yield Hash.make(user_data.password);
    yield new_user.save()

    response.json(new_user)
    return
  } 

  * get_gravatar_link(email) {
    return `https://www.gravatar.com/avatar/${md5(email)}.jpg?s=25`;
  }

}

module.exports = UserController
