'use strict'

module.exports = {
    //How often the interval must run
    increment_per_second: 3000,

    //How many users should be taken for top score
    top_score_count: 5
}
