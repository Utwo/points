'use strict'

const Factory = use('Factory')
const User = use('App/Model/User')
const Hash = use('Hash')

class UserTableSeeder {

    * run() {
        let user = yield User.all()
        yield* user.map(u => {
            u.delete()
        })

        yield this.create_user('user1', 'user1@example.com', '123456', 1)
        yield this.create_user('user2', 'user2@example.com', '123456', 1)
        yield this.create_user('user3', 'user3@example.com', '123456', 0)
    }

    * create_user(username, email, password, active){
        let user = new User()

        user.username = username
        user.email = email
        user.password = yield Hash.make(password)
        user.active = active

        yield user.save()
    }

}

module.exports = UserTableSeeder
